import math

class CovAll:
    def __init__(self, df=None, groupby=None, grouping=None, omit_xk=set(),
                 par_i=None, x_keys=None, k_xs=None,k_y=None):
        grouping = grouping or df.groupby(groupby)
        x_keys   = x_keys   or list(df.groupby(k_xs).count().index)
        assert grouping
        assert k_xs and k_y
        self.k_xs, self.k_y = k_xs, k_y
        self.n = 0
        print("Figuring parameter indices..")
        self.par_i = par_i or {xk:i for i,xk in enumerate(x_keys) if not xk in omit_xk}
        self.i_par = {self.par_i[k]:k for k in self.par_i}

        self.cov_sums = [[(0,0,0,0,0,0) for i in range(len(self.par_i))] for j in range(len(self.par_i))]

        print("Calculating correlations.. (between {} params)".format(len(self.par_i)))
        for groupkey, cdf in grouping:
            self.enter_raw(zip(*[cdf[k] for k in k_xs]), cdf[k_y])

    def g_par_i(self, k):
        got = self.par_i.get(k, None)
        if got: return got

        for k, i in self.par_i.items():
            if k[0] == k: return i

    def enter_raw(self, xk, gy):
        self.n += 1
        n = self.n
        if n==10 or n==100 or n%1000 == 0: print(n)  # TODO better do it every some number of seconds.
        il = [(self.par_i[tuple(k)], val) for k,val in zip(xk, gy) if tuple(k) in self.par_i]
        cov_sums = self.cov_sums
        for i,val1 in il:
            for j,val2 in il:
                nij, sij,si,sj,si2,sj2 = cov_sums[i][j]
                cov_sums[i][j] = (nij+1, sij + val1*val2, si + val1, sj + val2,
                                  si2 + val1*val1, sj2 + val2*val2)

    def variance(self):
        """Return the variance of different parameters. Not the varianced used for correlation
        because each correlation should not include to missing values relevant for that
        correlation."""
        return [sii/n - (si/n)**2 if n>0 else 0
                for (n, sii,si,sj,si2,sj2) in (self.cov_sums[i][i] for i in range(cnt))]

    def correlation(self):
        """Calculates correlation matrix.(fairly quick)"""
        cnt= len(self.i_par)
        return [[     ((sij/n - si*sj/(n*n))/math.sqrt((si2/n-(si/n)**2)*(sj2/n-(sj/n)**2))
                        if (n>0 and (si2/n-(si/n)**2)*(sj2/n-(sj/n)**2)>1e-3) else 0)
                        for n, sij,si,sj,si2,sj2 in (self.cov_sums[i][j] for i in range(cnt))]
                for j in range(cnt)]

    def sub_corr(self, pars):
        """Correlation matrix for subset of data, so you can focus on more important parts."""
        il = [self.par_i[p] for p in pars]
        corr = self.correlation()
        return [[corr[i][j] for i in il] for j in il]

    def corr_list_gen(self, min_cases=100):
        """Generator for listed correlations, unsorted."""
        i_par = self.i_par
        cnt, corr = len(i_par), self.correlation()
        return ((i_par[i],i_par[j], corr[i][j], self.cov_sums[i][j]) for i,j in
                ((math.floor(k/cnt),k%cnt) for k in range(cnt*cnt) if math.floor(k/cnt)>k%cnt)
                if self.cov_sums[i][j][0] > min_cases and i!=j)

    def corr_list(self, min_cases=100):
        """Sorted list of strongest correlations with minimum number of seen cases."""
        lst = list(self.corr_list_gen(min_cases=min_cases))
        lst.sort(key=lambda e:abs(e[2]), reverse=True)
        return lst

    def corr_gen_outward(self, key_list, min_cor=0.5, max_depth=3, min_cnt=200, max_get=1000):
        """Work outward from list of keys."""
        par_i, i_par = self.par_i, self.i_par
        corr = self.correlation()
        cov_sums = self.cov_sums
        seen_set = set()
        next_set = set(par_i[k] for k in key_list if k in par_i)
        permit_set = set(list(next_set))
        ij_corr = set()
        for n in range(max_depth):
            collect_set = set()
            for i in next_set:
                for j in i_par:
                    if i!=j and abs(corr[i][j]) > min_cor and cov_sums[i][j][0] > min_cnt:
                        # This does not fail, yet unconnected bits are not returned... Do not understand.
                        assert i in permit_set
                          # Should be superfluous, but seeing doubles..
                        if ((i,j) not in ij_corr) and (((j,i) not in ij_corr)):
                            ij_corr.add((i,j))
                        if (j not in seen_set) and (j not in next_set):
                            permit_set.add(j)
                            collect_set.add(j)

            if len(ij_corr) > max_get: break

            seen_set.union(next_set)
            next_set = collect_set

        return ((i_par[i],i_par[j], corr[i][j], self.cov_sums[i][j]) for i,j in ij_corr)

    def corr_list_outward(self, key_list, min_cor=0.5, max_depth=3, min_cnt=200):
        lst = list(self.corr_gen_outward(key_list, min_cor, max_depth, min_cnt))
        lst.sort(key=lambda e:abs(e[2]), reverse=True)
        return lst
